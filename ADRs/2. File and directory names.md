# Acuerdos de nombres de archivos y directorios

1. Cada directorio llevará el nombre del estudiante. Ejemplo: joe-doe
2. Los archivos de tipo html y css llevarán nombres conformados por una cadena en donde los espacios en blanco son representados por guiones. Ejemplo: joe-doe.html
3. Directorio de estilos se debe llamar css
4. Directorio de javascripts se debe llamar javascript
5. Directorio de fuentes se debe llamar fonts